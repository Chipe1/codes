/*
  Min. heap
  Zero indexed
*/

typedef struct  __struct_HeapV{
    long long val;
}HeapV;

HeapV Heap_makeV(long long);
int Heap_cmp(HeapV, HeapV);
void Heap_swap(HeapV *, int, int);
void Heap_heapify(HeapV *, int, int);
void Heap_rev_heapify(HeapV *, int);
void Heap_insert(HeapV *, HeapV, int *);
HeapV Heap_pop(HeapV *, int *);
void Heap_sort(HeapV *, int);

HeapV Heap_makeV(long long val){
    /*
      returns the struct which is used by the heap
    */
    HeapV retval;
    retval.val = val;
    return retval;
}

int Heap_cmp(HeapV a, HeapV b){
    /*
      Compares the values of a and b
      -1(less than zero) if a < b
      0 if a == b
      1(greater than zero) if a > b
    */
    if(a.val < b.val)
	return -1;
    else if(a.val > b.val)
	return 1;
    else
	return 0;
}

void Heap_swap(HeapV *ar, int i, int j){
    HeapV tmp;
    tmp = ar[i];
    ar[i] = ar[j];
    ar[j] = tmp;
}

void Heap_heapify(HeapV *ar, int zero_ind, int arsize){
    /*
      Push node down the tree to fix heap property
    */
    int one_ind;
    one_ind = zero_ind + 1;
    if(one_ind<<1 > arsize){
	// leaf node
	return;
    }
    
    int i;
    i = (one_ind<<1) - 1;  //left child
    if(i + 1 < arsize){
	// right child exists
	if(Heap_cmp(ar[i], ar[i + 1]) > 0){
	    i++;
	}
    }
    
    if(Heap_cmp(ar[zero_ind], ar[i]) > 0){
	Heap_swap(ar, zero_ind, i);
	Heap_heapify(ar, i, arsize);
    }
}

void Heap_rev_heapify(HeapV *ar, int ind){
    /*
      Push node up the tree to fix heap property
    */
    int parent;
    while(ind > 0){
	parent = ((ind + 1)>>1) - 1;
	if(Heap_cmp(ar[parent], ar[ind]) > 0){
	    Heap_swap(ar, ind, parent);
	    ind = parent;
	}
	else{
	    break;
	}
    }
}

void Heap_insert(HeapV *ar, HeapV val, int *arsize){
    /*
      Adds 'val' to the heap
    */
    ar[*arsize] = val;
    Heap_rev_heapify(ar, (*arsize)++);
}

HeapV Heap_pop(HeapV *ar, int *arsize){
    /*
      Pop the top-most(minimum) of the heap
    */
    HeapV toret;
    toret = ar[0];
    ar[0] = ar[--(*arsize)];
    Heap_heapify(ar, 0, *arsize);
    return toret;
}

void Heap_sort(HeapV *ar, int arsize){
    /*
      Heap sort
      Order is opposite to the heap priority
    */
    int i;
    for(i = arsize>>1; i >= 0; i--){
	Heap_heapify(ar, i, arsize);
    }
    while(arsize > 1){
	HeapV val;
	val = Heap_pop(ar, &arsize);
	ar[arsize] = val;
    }
}
